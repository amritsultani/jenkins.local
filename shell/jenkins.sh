#!/bin/bash

# yum -y update

yum -y install java # yum is analogous to apt-get
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo # download from web, jenkins repository
rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key # get the key to verifiy the authenticity of the software
yum -y install jenkins
systemctl start jenkins.service # start the jenkins service
# service jenkins start
